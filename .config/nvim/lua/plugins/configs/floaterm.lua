vim.g.floaterm_wintitle = false
vim.g.floaterm_width = 0.85
vim.g.floaterm_height = 0.85
vim.g.floaterm_position = 'center'
vim.g.floaterm_borderchars = {'','','','','','','',''}
--vim.g.floaterm_rootmarkers = {'git'}
vim.g.floaterm_title = ' $1/$2'
vim.cmd[[
hi Floaterm guibg=none
hi FloatermBorder guibg=none guifg=none
]]
