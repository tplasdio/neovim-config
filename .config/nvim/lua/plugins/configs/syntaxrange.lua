-- TODO: mejorar esto, buscar una lista donde neovim mantenga los ft (/usr/share/nvim/runtime/filetype.vim)
vim.g.Sxs = function(delimiter)
	delimiter = delimiter or '#'
	local ft
	local fts = { "vim", "m4", "sh", "c", "cpp", "python", "javascript", "r", "perl", "awk", "sed" }
	for _,ft in ipairs(fts) do
		vim.fn['SyntaxRange#Include'](delimiter..'vimsx='..ft, delimiter..'vimsx'..delimiter, ft, 'NonText')
	end
end

vim.g.Sx = function(p1, delimiter)
	delimiter = delimiter or '#'
	vim.fn['SyntaxRange#Include'](delimiter..'vimsx='..p1, delimiter..'vimsx'..delimiter, p1, 'NonText')
end
