-- DAP (Depuración):
require("plugins.configs.lang.dap")

-- nvim-gdb:
require("plugins.configs.lang.nvim-gdb")

-- cmp, autocompletion:
require("plugins.configs.completion.cmp")

-- LSP configurations:
require("plugins.configs.lang.lsp")

-- vim-illuminate:
require("plugins.configs.vim-illuminate")

-- changes:
-- require("plugins.configs.changes")

-- git-signs
require("plugins.configs.gitsigns")

-- BQF, mejor QuickFix:
require("plugins.configs.bqf")

-- Cmdbuf:
--require("plugins.configs.cmdbuf")

-- Neotex:
--require("plugins.configs.neotex")

-- vim-latex-live-preview {{{
--vim.g.livepreview_previewer = 'zathura'
-- }}}

-- Sniprun
--require("plugins.configs.sniprun")

-- Lualine:
require("plugins.configs.lualine")

-- Quick-scope:
require("plugins.configs.quick-scope")

-- SyntaxRange:
require("plugins.configs.syntaxrange")

-- Coqtail:
require('plugins.configs.coqtail')

-- Undotree:
require('plugins.configs.undotree')

-- Emmet:
require('plugins.configs.emmet')

-- Colorizer
require('plugins.configs.nvim-colorizer')

-- Gruvbox:
require('plugins.configs.gruvbox')

--AutoPairs:
require('plugins.configs.autopairs')

-- Vim_visual_multi:
require('plugins.configs.vim_visual_multi')

-- Floaterm:
require('plugins.configs.floaterm')

-- Vimwiki:
--require('plugins.configs.vimwiki')

-- mkdnflow:
require('plugins.configs.mkdnflow')

-- Telescope.nvim (fuzzy finder):
require('plugins.configs.telescope')

-- Nvim-tree:
-- Muy inestable, quizás lo vuelva a usar luego, pero no ahora
--require("plugins.configs.nvim-tree")

-- Nvim-treesitter:
require("plugins.configs.treesitter")

-- Nvim-treesitter-context:
require("plugins.configs.treesitter-context")
