require("lualine").setup {
	options = {
		icons_enabled = true,
		theme = 'pastelbox',  -- at lua/lualine/themes/
		component_separators = { left = '', right = ''},
		section_separators = { left = '', right = ''},
		disabled_filetypes = {},
		always_divide_middle = true,
	},
	sections = {
		lualine_a = {
			{
				'mode',
				hide_filename_extension = true,
				fmt = function(str) return str:sub(1,1) end,
			}
		},
		lualine_b = {
			{
				'branch',
				icon = {
					'', color = { fg = 'orange'}
				}
			},
			{
				'diff',
				symbols = { added = ' ', modified = '柳', removed = ' ' }, -- Changes the symbols used by the diff.
				diff_color = {
					modified = { fg = 'orange' },
					removed = { fg = 'red' },
				},
				source = nil, -- A function that works as a data source for diff.
				-- It must return a table as such:
				--   { added = add_count, modified = modified_count, removed = removed_count }
				-- or nil on failure. count <= 0 won't be displayed.
			},
		},

	--https://github.com/nvim-lualine/lualine.nvim/issues/479
		--{'diagnostics', sources={'nvim_lsp', 'coc'}}},
		lualine_c = {
			{
				'filename',
				symbols = {
					modified = ' ✏',
					readonly = ' 📕',
					unnamed = '📛'
				},
			}
		},
		lualine_x = {'fileformat', 'filetype'},
		lualine_y = {'progress'},
		lualine_z = {'location'}
	},
	inactive_sections = {
		lualine_a = {},
		lualine_b = {},
		lualine_c = {
			{
				'filename',
				symbols = {
					modified = ' ✏',
					readonly = ' 📕',
					unnamed = '📛'
				},
			}
		},
		lualine_x = {'location'},
		lualine_y = {},
		lualine_z = {}
	},
	tabline = {
		lualine_a = {
			{
				'tabs',
				hide_filename_extension = true,
				symbols = {
					modified = ' ✏',
					readonly = ' 📕',
					unnamed = '📛'
				},
			}
		},
		lualine_b = {
			{
				'filename',
				hide_filename_extension = true,
				symbols = {
					modified = ' ✏',
					readonly = ' 📕',
					unnamed = '📛'
				},
			}
		},
		lualine_c = {},
		lualine_x = {},
		lualine_y = {},
		lualine_z = {
			{
				'buffers',
				hide_filename_extension = true,
				symbols = {
					modified = ' ✏',
					unnamed = '📛', -- Bug: no cambia
					alternate_file = '',
					readonly = ' 📕',
				},
			}
		},
	},

	extensions = {}
}
