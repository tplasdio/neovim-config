local g = vim.g

g.VM_leader = '´'
g.VM_mouse_mappings = 1
g.VM_theme = 'iceblue'
g.VM_maps = {
	Undo = 'u',
	Redo = 'U',
	["Add Cursor At Pos"] = '´´',
	["Mouse Cursor"] = '<A-LeftMouse>'
}

g.VM_custom_motions = {
	j='h',
	l='j',
	["ñ"]='l',
	w='b',
	e='w',
	E='e',
	W='ge'
}
