require('sniprun').setup({
	display = {
		-- "VirtualText",
		"TempFloatingWindow",
		-- "LongTempFloatingWindow",
		 "Terminal",
		-- "TerminalWithCode",
		-- "NvimNotify",
	},

	snipruncolors = {
		SniprunVirtualTextOk  = { bg="#54ff54",fg="#000000",ctermbg="Cyan",cterfg="Black" },
		SniprunFloatingWinOk  = { fg="#54ff54",ctermfg="Cyan" },
		SniprunVirtualTextErr = { bg="#ca1f1f",fg="#000000",ctermbg="Red",cterfg="Black" },
		SniprunFloatingWinErr = { fg="#ca1f1f",ctermfg="Red" },
	},

	display_options = {
		terminal_width = 45,
	},
})
