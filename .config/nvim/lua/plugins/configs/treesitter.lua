require("nvim-treesitter.configs").setup {
	sync_install = false,
	highlight = {
		enable = true,
		disable = { "bash","perl","yaml","sql","latex" },
		-- bash: heredocs no son correctamente manejados: https://github.com/tree-sitter/tree-sitter-bash/issues/98
		-- python: docstrings no son correctamente reconocidos si se les pone debajo de un elif?
		--  los 'elifs()' cuando hay muchos, son coloreados como si fueran funciones
		-- perl: simplemente mal: variables como palabras reservadas, arreglos como funciones, no doctrings ni bucle for, etc.
		-- yaml: si una secuencia [], mapa {} o string '', "" expande múltiples líneas, el resaltado se rompe aunque sea sintaxis yaml válida
		additional_vim_regex_highlighting = false, -- true si quieres correr :h syntax y tree-sitter a la vez, no recomendado
	},

	-- Seleccionar partes relevantes de código incrementalmente, o sea
	-- selecciona un string/paréntesis/función, etc. (muy útil y cómodo)
	incremental_selection = {
		enable = true,
		keymaps = {
			init_selection = '<CR>',
			scope_incremental = '<CR>',
			node_incremental = '<M-v>',
			node_decremental = '<M-S-v>',
		},
	},

	-- nvim-ts-rainbow, para paréntesis de colores
	rainbow = {
		enable = true,
		--extended_mode = true,
		max_file_lines = nil,
		colors = {
			"#fe8019",
			"#ff3370",
			"#92ff49",
			--"#60b5ff",
			--"#6affa5",
		},
	}
}
