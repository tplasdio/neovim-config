require('bqf').setup({
	auto_enable = true,
	auto_resize_height = true, -- highly recommended enable
	preview = {
		win_height = 12,
		win_vheight = 12,
		delay_syntax = 80,
		--border_chars = {'┃', '┃', '━', '━', '┏', '┓', '┗', '┛', '█'},
		should_preview_cb = function(bufnr, qwinid)
			local ret = true
			local bufname = vim.api.nvim_buf_get_name(bufnr)
			local fsize = vim.fn.getfsize(bufname)
			if fsize > 100 * 1024 then
				-- skip file size greater than 100k
				ret = false
			elseif bufname:match('^fugitive://') then
				-- skip fugitive buffer
				ret = false
			end
			return ret
		end
	},
	-- make `drop` and `tab drop` to become preferred
	func_map = {
		pscrollup = 's',
		pscrolldown = 'd',
		split = '<M-i>',
		vsplit = '<M-o>',
	},
	filter = {
		fzf = {
			action_for = {['alt-o'] = 'split', ['alt-i'] = 'vsplit'},
			extra_opts = {'--bind', 'ctrl-o:toggle-all', '--prompt', '> '}
		}
	}
})
