local tel = require("telescope")

tel.load_extension("file_browser")

tel.setup{
	defaults = {
		mappings = {
			i = {
				-- :help telescope.defaults.mappings
				["<M-l>"] = "move_selection_next",
				["<M-k>"] = "move_selection_previous",
				--["<C-c>"] = "close",
				["<M-q>"] = "close",
				["?"] = "which_key"
			},
			n = {
				["l"] = "move_selection_next",
				["k"] = "move_selection_previous"
			}
		},
	},
	extensions = {
		file_browser = {
			--theme = "ivy",
			hijack_netrw = true,
			--mappings = {
				--i = {  } -- Modo insertar
				--n = {  } -- Modo normal
			--}
		}
	},
	border = false,
}

