-- TODO: que en wikis no se sobremapee '-' con VimwikiRemoveHeaderLevel, ver ':verbose nmap'
local wiki_principal = {
	path = vim.env.HOME .. '/docs/vimwiki/',
	name = 'Wiki_principal',
	auto_export = 0,
	syntax = 'default',
	ext = 'wiki',
	links_space_char = '_',
	path_html = vim.env.HOME .. '/docs/vimwiki_html/'
}

local wiki_notas = {
	path = vim.env.HOME .. '/docs/notas/',
	name = 'Wiki_notas',
	auto_export = 0,
	syntax = 'default',
	ext = 'wiki',
	links_space_char = '_',
	path_html = vim.env.HOME .. '/docs/notas_html/'
}

vim.g.vimwiki_list = {wiki_principal,wiki_notas}
