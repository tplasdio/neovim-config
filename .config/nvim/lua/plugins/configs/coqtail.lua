-- Coqtail
local setkey1 = vim.api.nvim_set_keymap
local noremap = { noremap = true }
vim.g.coqtail_nomap = 1
setkey1('n','<leader>cc',':CoqStart', noremap)
setkey1('n','<leader>cq',':CoqStop', noremap)
setkey1('n','<leader>ck',': noremapCoqUndo', noremap)
setkey1('n','<leader>cl',': noremapCoqNext', noremap)
setkey1('n','<leader>cg',': noremapCoqToLine', noremap)
setkey1('n','<leader>cgg',':CoqToTop', noremap)
setkey1('n','<leader>cG',':CoqJumpToEnd', noremap)
