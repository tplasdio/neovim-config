-- LSP setup configuration
local setkey2 = vim.keymap.set

vim.lsp.protocol.CompletionItemKind = {
	" (Text) ",
	" (Method)",
	" (Function)",
	" (Constructor)",
	"ﴲ (Field)",
	" (Variable)",
	" (Class)",
	"ﰮ (Interface)",
	" (Module)",
	"襁(Property)",
	" (Unit)",
	" (Value)",
	"練(Enum)",
	" (Keyword)",
	" (Snippet)",
	" (Color)",
	" (File)",
	" (Reference)",
	" (Folder)",
	" (EnumMember)",
	"ﲀ (Constant)",
	"ﳤ (Struct)",
	" (Event)",
	" (Operator)",
	" (TypeParameter)",
}

local lsp_signs = { Error = " ", Warn = " ", Hint = " ", Info = " " }
for type, icon in pairs(lsp_signs) do
  local hl = "DiagnosticSign" .. type
  vim.fn.sign_define(hl, { text = icon, texthl = hl, numhl = hl })
end

vim.o.signcolumn = "number"  -- Si quieres que siempre haya una columna izquierda vacía donde estén los símbolos

-- Configuración de diagnósticos LSP
vim.diagnostic.config({
	--virtual_text = { prefix = '▎' },   -- Caracter separando texto virtual
	virtual_text = false,
	underline = false,
	update_in_insert = true,
})

local lsp_diagnostics_opts = {
	scope="line",
	border = 'rounded',
	source = 'always',
	prefix = ' ',
}

-- Mostrar diagnósticos LSP en ventana flotante
--vim.o.updatetime = 180
--*** ↑ PARECE QUE ES ESTO LO QUE CAUSA LAG

--[[
local function diagnostic_window_on_event(event)
	vim.api.nvim_create_autocmd(event, {
		buffer = bufnr,
		callback = function()
			local opts = {
				focusable = false,
				close_events = { "BufLeave", "CursorMoved", "InsertEnter", "FocusLost" },
				border = 'rounded',
				source = 'always',
				prefix = ' ',
				scope = 'line',
			}
			vim.diagnostic.open_float(nil, opts)
		end
	})
end
diagnostic_window_on_event("CursorHold")
--]]

-- Ventana flotante de ayuda de funciones

vim.cmd [[autocmd! ColorScheme * hi! NormalFloat guibg=none | hi! FloatBorder guibg=none]]

local lsp_border = {
	{"╭", "FloatBorder"},
	{"─", "FloatBorder"},
	{"╮", "FloatBorder"},
	{"│", "FloatBorder"},
	{"╯", "FloatBorder"},
	{"─", "FloatBorder"},
	{"╰", "FloatBorder"},
	{"│", "FloatBorder"},
}

local lsp_handlers =  {
	["textDocument/hover"] =  vim.lsp.with(vim.lsp.handlers.hover, {border = lsp_border}),
	["textDocument/signatureHelp"] =  vim.lsp.with(vim.lsp.handlers.signature_help, {border = lsp_border }),
}

local function lsp_illuminate_same_morphemes()
	--require("illuminate").on_attach(client)
	if client.resolved_capabilities.document_highlight then
		vim.cmd [[
		hi! LspReferenceRead  cterm=bold gui=underline
		hi! LspReferenceText  cterm=bold gui=underline
		hi! LspReferenceWrite cterm=bold gui=underline
		]]
		vim.api.nvim_create_augroup('lsp_document_highlight', {
			clear = false
		})
		vim.api.nvim_clear_autocmds({
			buffer = bufnr,
			group = 'lsp_document_highlight',
		})
		vim.api.nvim_create_autocmd({ 'CursorHold', 'CursorHoldI' }, {
			group = 'lsp_document_highlight',
			buffer = bufnr,
			callback = vim.lsp.buf.document_highlight,
		})
		vim.api.nvim_create_autocmd('CursorMoved', {
			group = 'lsp_document_highlight',
			buffer = bufnr,
			callback = vim.lsp.buf.clear_references,
		})
	end
end

-- LSP keybindings: {{{

-- LSP on_attach function
local lsp_attach = function(client)
	print('LSP started.')
	setkey2('n','gw',function() vim.lsp.buf.document_symbol() end)  -- Símbolos del código actual (variables, funciones, clases, etc.) en ventana 'QuickFix'
	setkey2('n','gD',function() vim.lsp.buf.declaration() end)  -- Ir al código de la declaración
	setkey2('n','gd',function() vim.lsp.buf.definition() end)  -- Ir al código de la definición (puede ser otro fichero)
	setkey2('n','K',function() vim.lsp.buf.hover() end)  -- Mostrar ayuda de función/clase
	setkey2('n','gr',function() vim.lsp.buf.references() end)
	setkey2('n','gs',function() vim.lsp.buf.signature_help() end)
	setkey2('n','gi',function() vim.lsp.buf.implementation() end)
	setkey2('n','gt',function() vim.lsp.buf.type_definition() end)  -- Ir a código de definición?
	setkey2('n','<leader>lw',function() vim.lsp.buf.workspace_symbol() end)
	setkey2('n','<leader>lf',function() vim.lsp.buf.code_action() end)
	setkey2('n','<leader>le', function() vim.diagnostic.open_float(lsp_diagnostics_opts) end)
	setkey2('n','<leader>lr',function() vim.lsp.buf.rename() end)  -- Reescribir el nombre del código
	setkey2('n','<leader>=', function() vim.lsp.buf.formatting() end)
	setkey2('n','<leader>li',function() vim.lsp.buf.incoming_calls() end)
	setkey2('n','<leader>lo',function() vim.lsp.buf.outgoing_calls() end)

	-- Subrayar mismos morfemas
	lsp_illuminate_same_morphemes()

	require('completion').on_attach(client)
	require('diagnostic').on_attach(client)
end

-- }}}

-- LSP initialization with lspconfig (setup)
local lspconfig = require("lspconfig")

local ft_lsp = {   -- Filetype = name, { command, arg }
	python = {'pyright', {'pyright-langserver', "--stdio"}},
	c   = {'clangd', {'clangd'} },
	cpp = {'clangd', {'clangd'} },
	latex = {'texlab', {'texlab'} },
	bash = { 'bashls', { 'bash-language-server', 'start' } }
}

for ft, lsp_server in pairs(ft_lsp) do
	lspconfig[lsp_server[1]].setup {
		cmd = lsp_server[2],
		on_attach = lsp_attach,
		autostart = false,  -- Iniciarlos manualmente con :LspStart
		filetypes = { ft },
		capabilities = capabilities,
		handlers = lsp_handlers
	}
end

-- https://rust-analyzer.github.io/manual.html
-- https://sharksforarms.dev/posts/neovim-rust/
lspconfig.rust_analyzer.setup({
	on_attach = lsp_attach,
	autostart = false,
	filetypes = { 'rust' },
	settings = {
		["rust-analyzer"] = {
			imports = {
				granularity = { group = "module", },
				prefix = "self",
			},
			cargo = {
				buildScripts = { enable = true, },
			},
			procMacro = { enable = true },
		}
	}
})

vim.cmd([[
set completeopt=menuone,noinsert,noselect
let g:completion_matching_strategy_list = [ 'exact', 'substring', 'fuzzy' ]
]])

--vim.g.completion_matching_strategy_list = { 'exact', 'substring', 'fuzzy' }
