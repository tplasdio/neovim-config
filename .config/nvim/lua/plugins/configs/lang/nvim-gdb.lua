local setkey2 = vim.keymap.set
vim.g.nvimgdb_disable_start_keymaps = true
vim.g.nvimgdb_use_find_executables = 0
vim.g.nvimgdb_use_cmake_to_find_executables = 0

vim.g.nvimgdb_config_override = {
	key_until         = '<leader>du',
	key_continue      = '<leader>dc', -- (continue)
	key_next          = '<leader>do', -- Step over (next)
	key_step          = '<leader>di', -- Step into (step)
	key_finish        = '<leader>df',
	key_breakpoint    = '<leader>b',
	key_frameup       = '<leader>dk',
	key_framedown     = '<leader>dl',
	key_eval          = '<leader>de',
	key_quit          = '<leader>dq',
	termwin_command   = 'belowright vnew',
	codewin_command   = 'vnew',
	sign_current_line = '🏁',
	sign_breakpoint   = { '📌', '●²', '●³', '●⁴', '●⁵', '●⁶', '●⁷', '●⁸', '●⁹', '●ⁿ' },
}

function NvimGDB(ft)
	ft = ft or vim.bo.filetype
	-- TODO: que sea un switch
	if ft == "bash" or ft == "sh" then
		vim.api.nvim_exec('GdbStartBashDB bashdb %', true)  -- ← Pasar '--' para evitar resto de parseo de argumentos
	elseif ft == "python" then
		vim.api.nvim_exec('GdbStartPDB python -m pdb %', true)
	elseif ft == "c" or ft == "cpp" or ft == "nim" then
		local binfile = vim.fn.expand("%:r")
		local outbinfile = binfile .. ".out"
		if vim.fn.filereadable(outbinfile) == 1 then
			vim.api.nvim_exec('GdbStart gdb -q '..outbinfile, true)
		elseif vim.fn.filereadable(binfile) == 1 then
			vim.api.nvim_exec('GdbStart gdb -q '..binfile, true)
		elseif vim.fn.filereadable("a.out") == 1 then
			vim.api.nvim_exec('GdbStart gdb -q a.out', true)
		else
			error("No appropriate binary file found for debugging nvim-gdb")
		end
	else
		error("filetype not valid for nvim-gdb")
	end
	-- TODO: agregar un menú que permita pasar argumentos de línea de comandos, como lo hace DAP
end

setkey2('n','<leader>db',function() NvimGDB() end)
