local setkey2 = vim.keymap.set

require("dap-python").setup()

local dap, dapui = require("dap"), require("dapui")

-- ⭕✅🚥🚦💡⚓🚧📌🛑🏁🚩📋🔴
vim.fn.sign_define('DapBreakpoint', {text='📌', texthl='', linehl='', numhl=''})
vim.fn.sign_define('DapBreakpointCondition', {text='🚥', texthl='', linehl='', numhl=''})
vim.fn.sign_define('DapLogPoint', {text='📝', texthl='', linehl='', numhl=''})
vim.fn.sign_define('DapStopped', {text='🏁', texthl='', linehl='', numhl=''})
vim.fn.sign_define('DapBreakpointRejected', {text='❌', texthl='', linehl='', numhl=''})

local dap_attach = function(client)
	setkey2('n',"<leader>do", function() dap.step_over() end)  -- Siguiente instrucción
	setkey2('n',"<leader>di", function() dap.step_into() end)  -- Entrar en función
	setkey2('n',"<leader>dq", function() dap.step_out() end)  -- Salir de función
	setkey2('n',"<leader>dc", function() dap.set_breakpoint(vim.fn.input('Condición de corte: ')) end)   -- Establecer punto de corte condicional
	setkey2('n',"<leader>dB", function() dap.set_breakpoint(nil,nil,vim.fn.input('Punto de corte establecido')) end)
	setkey2('n',"<leader>dr", function() dap.repl.open() end)
	--setkey2('n',"<leader>drc", function() dap.console.open() end)
end
setkey2('n',"<leader>dd", function() dap.continue() end)  -- Iniciar DAP
setkey2('n',"<leader>b", function() dap.toggle_breakpoint() end)  -- Alternar punto de corte
setkey2('n',"<leader>db", function() dap.toggle_breakpoint(); dap.continue() end)  -- Poner punto de corte y continuar/iniciar depurador
setkey2('n',"<leader>du", function() dapui.open() end)  -- Abrir DAP-UI
setkey2('n',"<leader>de", function() dapui.eval() end)
setkey2('n',"<leader>dk", function() dapui.float_element("console") end)
dap_attach()
--dap.setup({on_attach = dap_attach})

dapui.setup({
	icons = { expanded = "📂", collapsed = "📁" },
	layouts = {
		{
			elements = {
				-- Elements can be strings or table with id and size keys.
				{ id = "scopes", size = 0.25 },
				"breakpoints",
				"stacks",
				"watches",
			},
			size = 40,
			position = "right",
		},
		{
			elements = {
				"console",
			},
			size = 0.20, -- 25% of total lines
			position = "bottom",
		},
	}
})

-- Automáticamente abrir el UI de DAP al iniciar DAP, y cerrarlo al término
dap.listeners.after.event_initialized["dapui_config"] = function() dapui.open() end
dap.listeners.after.event_terminated["dapui_config"] = function() dapui.close() end
dap.listeners.after.event_exited["dapui_config"] = function() dapui.close() end
