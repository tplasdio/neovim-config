-- :DC   Deshabilitar
-- :EC   Habilitar
-- :CL   QuickFix de cambios
-- [h    Anterior cambio
-- h]    Siguiente cambio
-- vah   Seleccionar cambio bajo cursor
-- <leader>h   Nivelar cambios bajo cursor con git
-- <leader>H   Desnivelar cambios bajo cursor con git

vim.g.changes_sign_text_utf8 = 1
-- Icons: 🟢 🔴 🟡 ⭐, tienes que cambiarlos en el código fuente, en 'InitSignDef'
-- TODO: forkear, hacerlos una opción y subirlos upstream
vim.g.changes_diff_preview = 0  -- Ventana de diferencias
-- TODO: que esto sea toggeable

vim.g.changes_linehi_diff = 1  -- Resaltar los cambios en la línea
-- Colores de resaltado, no funciona, cambialo en el código fuente
vim.api.nvim_command [[ hi ChangesSignTextAdd guifg=green  guibg=NONE ctermbg=NONE ctermfg=green ]]
vim.api.nvim_command [[ hi ChangesSignTextDel guifg=red    guibg=NONE ctermbg=NONE ctermfg=red ]]
vim.api.nvim_command [[ hi ChangesSignTextCh  guifg=orange guibg=NONE ctermbg=NONE ctermfg=orange ]]

-- Si es manejado por git, resaltar los cambios no comprometidos, no los no escritos
--vim.g.changes_vcs_check = 1
--vim.g.changes_vcs_system = 'git'
