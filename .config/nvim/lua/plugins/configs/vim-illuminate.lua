require('illuminate').configure({
	providers = {
		'lsp',
		'treesitter',
		'regex',
	},
	delay = 50,
	min_count_to_highlight = 2,
	under_cursor = false,  -- No subrayar la homónima bajo cursor, solo el resto
	--filetypes_denylist = { 'python', }
})
vim.api.nvim_command [[ hi def IlluminatedWordText  cterm=bold gui=underline,bold,italic ]]
vim.api.nvim_command [[ hi def IlluminatedWordRead  cterm=bold gui=underline,bold,italic ]]
vim.api.nvim_command [[ hi def IlluminatedWordWrite cterm=bold gui=underline,bold,italic ]]
