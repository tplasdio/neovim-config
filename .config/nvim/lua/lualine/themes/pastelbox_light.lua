-- Copyright (c) 2020-2021 shadmansaleh
-- MIT license, see LICENSE for more details.
-- stylua: ignore
local colors = {
  black        = '#1d2021',
  white        = '#eaf5f9',
  orange       = '#ff6019',
  green        = '#00a800',
  blue         = '#300f78',
  gray         = '#a5c6d5',
  darkgray     = '#1f2430',
  lightgray    = '#89a9c7',
  inactivegray = '#2b3242'
}

return {
  normal = {
    a = { bg = colors.darkgray, fg = colors.white, gui = 'bold' },
    b = { bg = colors.gray, fg = colors.darkgray },
    c = { bg = colors.lightgray, fg = colors.darkgray },
  },
  insert = {
    a = { bg = colors.blue, fg = colors.white, gui = 'bold' },
    b = { bg = colors.gray, fg = colors.darkgray },
    c = { bg = colors.gray, fg = colors.black },
  },
  visual = {
    a = { bg = colors.orange, fg = colors.white, gui = 'bold' },
    b = { bg = colors.gray, fg = colors.darkgray },
    c = { bg = colors.darkgray, fg = colors.white },
  },
  replace = {
    a = { bg = colors.green, fg = colors.white, gui = 'bold' },
    b = { bg = colors.gray, fg = colors.darkgray },
    c = { bg = colors.gray, fg = colors.black },
  },
  command = {
    a = { bg = colors.darkgray, fg = colors.white, gui = 'bold' },
    b = { bg = colors.gray, fg = colors.darkgray },
    c = { bg = colors.lightgray, fg = colors.darkgray },
  },
  inactive = {
    a = { bg = colors.lightgray, fg = colors.inactivegray },
    b = { bg = colors.lightgray, fg = colors.inactivegray },
    c = { bg = colors.lightgray, fg = colors.inactivegray },
  },
}
