-- Copyright (c) 2020-2021 hoob3rt
-- MIT license, see LICENSE for more details.
-- stylua: ignore
local colors = {
  black        = '#1d2021',
  darkgray     = '#282f3e',
  gray         = '#3a4359',
  inactivegray = '#45506a',
  lightgray    = '#a5c6d5',
  white        = '#e0f2f7',
  red          = '#9d0006',
  green        = '#006400',
  blue         = '#65cbf8',
  orange       = '#fa7f1a',
  darkorange   = '#c52c02',
  aqua         = '#005e5e',
  yellow       = '#fabd2f',
}

return {
  normal = {
    a = { bg = colors.black, fg = colors.lightgray, gui = 'bold' },
    b = { bg = colors.gray, fg = colors.white },
    c = { bg = colors.darkgray, fg = colors.white },
  },
  insert = {
	a = { bg = colors.black, fg = colors.lightgray, gui = 'bold' },
	b = { bg = colors.darkgray, fg = colors.white },
	c = { bg = colors.aqua, fg = colors.white },
  },
  visual = {
    a = { bg = colors.black, fg = colors.lightgray, gui = 'bold' },
    b = { bg = colors.darkgray, fg = colors.white },
    c = { bg = colors.red, fg = colors.white },
  },
  replace = {
    a = { bg = colors.black, fg = colors.white, gui = 'bold' },
    b = { bg = colors.darkgray, fg = colors.white },
    c = { bg = colors.darkorange, fg = colors.white },
  },
  command = {
    a = { bg = colors.black, fg = colors.white, gui = 'bold' },
    b = { bg = colors.darkgray, fg = colors.white },
    c = { bg = colors.green, fg = colors.white },
  },
  inactive = {
    a = { bg = colors.darkgray, fg = colors.lightgray, gui = 'bold' },
    b = { bg = colors.darkgray, fg = colors.lightgray },
    c = { bg = colors.darkgray, fg = colors.lightgray },
  },
}
