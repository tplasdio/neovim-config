#!/usr/bin/luajit

local call = vim.call

local abbrevs = {
	c = {
		["c"] = "call",
		["wr"] = "set wrap",
		["nowr"] = "set nowrap",
		["tw"] = "set tw",
		["pyx"] = "!python %",
		["sx"] = "set syntax",
		["sxm"] = 'call Sx',
		["ft"] = "set ft",
		["bv"] = "set virtualedit=block",
		["nvimed"] = "e $INITVIM",
		["bashed"] = "e $BASHRC",
		["chmod"] = "!chmod u+x '%:p'",
		["$$"] = "%:p",
		["wiki"] =  "VimwikiUISelect",
		["vb"] =  "\\%V",
		["tab"] = "call tab()",
		["tabs"] = "call tab()",
		["spaces"] = "call utab()",
		["utab"] = "call utab()",
		["pk"] = "call Paragraphkeys()"
	}
}

-- Abreviaciones: {{{
vim.cmd([[
fun! Abreviar(abbr, comando)
	exec 'cnoreabbrev <expr> '.a:abbr
				\ .' ((getcmdtype() is# ":" && getcmdline() is# "'.a:abbr.'")'
				\ .'? ("'.a:comando.'") : ("'.a:abbr.'"))'
endfun
]])

for k,v in pairs(abbrevs.c) do
	call('Abreviar', k, v)
end

-- }}}
