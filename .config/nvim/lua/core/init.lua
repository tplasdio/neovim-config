#!/usr/bin/luajit

local o = vim.o
local g = vim.g

-- Esquemas_de_color: {{{
vim.g.gruvbox_italic = 1
vim.g.gruvbox_italicize_strings = 1
-- }}}

local setkey1 = vim.api.nvim_set_keymap
local setkey2 = vim.keymap.set
local noremap = { noremap = true }
--local setkey1nr = function(...)
	--local argv = table.pack(...)
	--table.insert(noremap)
	--setkey1(unpack(...))
--end

-- Configuración general paquetes: {{{
require("plugins.configs")
--}}}

-- Autocomandos: {{{

-- Redimensionar neovim al ser abierto por terminal '-e':
-- https://github.com/neovim/neovim/issues/11330
vim.api.nvim_create_autocmd({"VimEnter"}, {
  callback = function()
    local pid, WINCH = vim.fn.getpid(), vim.loop.constants.SIGWINCH
    vim.defer_fn(function() vim.loop.kill(pid, WINCH) end, 20)
  end
})

-- Guardar automáticamente envolveduras
vim.cmd([[
augroup remember_folds
	autocmd!
	autocmd BufWinLeave,BufLeave ?* silent! mkview
	autocmd BufWinEnter ?* silent! loadview
augroup END
]])

-- Sintaxis markdown en ficheros de calcurse
vim.cmd([[
autocmd BufRead,BufNewFile /tmp/calcurse*,~/.calcurse/notes/* set ft=vimwiki
"autocmd BufWritePre * %s/\s\+$//e  "Remover espacios finales al guardar
]])

--Alternar el poner un color de fondo con C-x C-t
vim.cmd([[
autocmd vimenter * hi normal guibg=NONE ctermbg=NONE
]])

-- Esto deja algunos caracteres con fondo de color, no como ↑
--vim.api.nvim_create_autocmd({"vimenter"}, {
	--pattern = {"*"},
	--callback = function()
		--vim.api.nvim_set_hl(0, "Normal", {
			--bg = "NONE",
			--ctermbg = "NONE"
		--})
	--end
--})

is_transparent = false
local function Alternar_fondo_transparente()
	-- Bug, por alguna razón al cambiar el fondo el color de las letras cambia de #def4f7 a #fff5d3
	if is_transparent == false then
		vim.api.nvim_set_hl(0, 'Normal', {
			bg="#1f2430",
			ctermbg="black"
		})
		is_transparent = true
	else
		vim.api.nvim_set_hl(0, 'Normal', {
			bg=nil, ctermbg=nil
		})
		is_transparent = false
	end
end
setkey2('n','<C-x><C-t>',function() Alternar_fondo_transparente() end)

-- Tabbing preferences

g.tab = function(n, expand)
	n = n or 4
	expand = expand or false
	o.expandtab = expand
	o.shiftwidth = n
	o.tabstop = n
	o.softtabstop = n
end

g.utab = function(n)
	n = n or 4
	g.tab(n, true)
end

-- Abrir páginas de ayuda verticalmente
vim.cmd([[
augroup vimrc_help
  autocmd!
  autocmd BufEnter *.txt if &buftype == 'help' | wincmd L | endif
augroup END
]])
-- Alternativamente: vert bo help


-- Pegar modeline de configs actuales al final
--vim.g.AppendModeline = function()
	--local modeline = io.write(string.format(" vim: set ft=%s ts=%d sw=%d tw=%d %set :",
		
	--))
	--modeline = [>vim.fn['substitute'](, "%s", modeline, "")<]
	--vim.fn['append'](line("$"), modeline)
--end
vim.cmd([[
function! AppendModeline()
  let l:modeline = printf(" vim: set ft=%s ts=%d sw=%d tw=%d %set :",
        \ &filetype, &tabstop, &shiftwidth, &textwidth, &expandtab ? '' : 'no')
  let l:modeline = substitute(&commentstring, "%s", l:modeline, "")
  call append(line("$"), l:modeline)
endfunction
]])

-- }}}

--}}}

-- Para undo persistente {{{
if vim.fn.has('persistent_undo') == 1 then
	vim.o.undodir = vim.env.XDG_CACHE_HOME .. '/nvim/undotree'
	vim.o.undofile = true
end
-- }}}


-- Formato para menú QuickFix {{{
-- Sacado de: https://github.com/kevinhwang91/nvim-bqf
function _G.qftf(info)
	local items
	local ret = {}
	if info.quickfix == 1 then
		items = vim.fn.getqflist({id = info.id, items = 0}).items
	else
		items = vim.fn.getloclist(info.winid, {id = info.id, items = 0}).items
	end
	local limit = 31
	local fnameFmt1, fnameFmt2 = '%-' .. limit .. 's', '…%.' .. (limit - 1) .. 's'
	local validFmt = '%s │%5d:%-3d│%s %s'
	for i = info.start_idx, info.end_idx do
		local e = items[i]
		local fname = ''
		local str
		if e.valid == 1 then
			if e.bufnr > 0 then
				fname = vim.fn.bufname(e.bufnr)
				if fname == '' then
					fname = '[No Name]'
				else
					fname = fname:gsub('^' .. vim.env.HOME, '~')
				end
				-- char in fname may occur more than 1 width, ignore this issue in order to keep performance
				if #fname <= limit then
					fname = fnameFmt1:format(fname)
				else
					fname = fnameFmt2:format(fname:sub(1 - limit))
				end
			end
			local lnum = e.lnum > 99999 and -1 or e.lnum
			local col = e.col > 999 and -1 or e.col
			local qtype = e.type == '' and '' or ' ' .. e.type:sub(1, 1):upper()
			str = validFmt:format(fname, lnum, col, qtype, e.text)
		else
			str = e.text
		end
		table.insert(ret, str)
	end
	return ret
end

vim.o.qftf = '{info -> v:lua._G.qftf(info)}'
--}}}


-- Tipos de ficheros (mejor poner esto en el filetype.vim, quizá en .local/share/nvim/site/after??)
--vim.cmd([[
--au BufRead *.bash setf bash
--au BufRead *.zsh setf zsh
--]])
