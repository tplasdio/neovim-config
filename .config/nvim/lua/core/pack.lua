#!/usr/bin/luajit

-- Gestión de paquetes: {{{


require('packer').startup(function()
	use 'wbthomason/packer.nvim'  -- Gestor de paquetes
	use 'mbbill/undotree'  -- Árbol de deshacer
	use 'preservim/nerdcommenter'  -- Comentarios
	use 'jiangmiao/auto-pairs'  -- Autopares ""''{}(), etc.
	use 'tpope/vim-surround'  -- Encerrador
	use 'vim-scripts/vis'  -- Comandos sobre bloque visual

	-- Movimiento
	use 'unblevable/quick-scope'  -- Resaltado con f,F,t,T
	use 'chaoren/vim-wordmotion'

	use {'mg979/vim-visual-multi', branch = 'master' }  -- Múltiples cursores

	use { "ellisonleao/gruvbox.nvim" } -- Tema
	use {'norcalli/nvim-colorizer.lua'}
	use 'RRethy/vim-illuminate'  -- Iluminar palabras homónimas
	use 'mattn/emmet-vim'  -- HTML

	--use 'vimwiki/vimwiki'  -- Wiki
	--use { "vimwiki/vimwiki", branch = "dev", }
	-- https://old.reddit.com/r/neovim/comments/r9pto6/help_vimwiki_html_issue/
	use { 'jakewvincent/mkdnflow.nvim', rocks = 'luautf8', }

	--use 'iamcco/markdown-preview'  -- Markdown en navegador

	--use 'vmchale/ion-vim'
	--use {'kevinhwang91/nvim-bqf', ft = 'qf'}  -- Mejor ventana QuickFix
	use 'kevinhwang91/nvim-bqf'  -- Mejor ventana QuickFix
	use {'notomo/cmdbuf.nvim' } -- Mejor ventana de comandos de vim (q:)
	--use 'c60cb859/bufMov.nvim'  -- Intercambiar búfers, conflictivo con algun otro plugin
	--use 'chrisbra/changesPlugin'  -- Señalar cambios no escritos en fichero abierto y git

	-- Terminal flotante
	use 'voldikss/vim-floaterm'
	-- Dependencias: neovim-remote

	-- Git
	--use { 'TimUntersberger/neogit', requires = 'nvim-lua/plenary.nvim' } -- Cliente git
	--use 'tpope/vim-fugitive'
	use {
		'lewis6991/gitsigns.nvim',
		requires = 'nvim-lua/plenary.nvim',
		config = [[require('config.gitsigns')]],
	}

	use {
		'akinsho/git-conflict.nvim',
		tag = '*',
		config = function()
			require('git-conflict').setup()
		end,
	}

	-- LSP (parseo de lenguajes). Debes instalar servidores LSP para tus lenguajes
	use 'neovim/nvim-lspconfig'

	-- DAP (depurador). Debes instalar servidores DAP para tus lenguajes
	use 'mfussenegger/nvim-dap'
	use 'mfussenegger/nvim-dap-python'
	use { "rcarriga/nvim-dap-ui", requires = {"mfussenegger/nvim-dap"} }
	use 'theHamsta/nvim-dap-virtual-text'
	--use 'nvim-telescope/telescope-dap.nvim'

	use 'sakhnik/nvim-gdb'  -- depuradores para C/C++ (GBD), Python (PDB), Bash (BashDB)

	-- Explorador de código, con LSP
	--use({
		--'ray-x/navigator.lua',
		--requires = {
			--{ 'ray-x/guihua.lua', run = 'cd lua/fzy && make' },
			--{ 'neovim/nvim-lspconfig' },
		--},
	--})

	-- cmp, autocompletado
	use 'hrsh7th/cmp-nvim-lsp'
	use 'hrsh7th/cmp-buffer'
	use 'hrsh7th/cmp-nvim-lsp'
	use 'hrsh7th/cmp-path'
	use 'hrsh7th/cmp-cmdline'
	use 'hrsh7th/nvim-cmp'

	-- Jupyter-like interactive execution
	use { 'dccsillag/magma-nvim', run = ':UpdateRemotePlugins' }
	-- Dependencias: pynvim


	-- Más resaltados de sintaxis

	-- Resaltado de sintaxis mixto
	use 'inkarkat/vim-SyntaxRange'
	use 'inkarkat/vim-ingo-library'

	-- Parseador de lenguajes, para sintaxis y resaltado
	use {
		'nvim-treesitter/nvim-treesitter'
		--run = ':TSUpdate'  # Recomendable usarlo, pues cuando actualices algo se puede romper, e.g.: https://github.com/nvim-treesitter/nvim-treesitter/issues/759
	}

	use 'p00f/nvim-ts-rainbow'
	use 'nvim-treesitter/nvim-treesitter-context'

	use 'shmup/vim-sql-syntax'
	use 'LhKipp/nvim-nu'  -- run = ':TSInstall nu'

	--use 'vim-scripts/lrc.vim' -- .lrc (letras)

	--use 'whonore/Coqtail' -- coqide-like
	--use 'jlapolla/vim-coq-plugin'  -- coq

	-- Gestor de ficheros
	use {
		'kyazdani42/nvim-tree.lua',
		requires = {'kyazdani42/nvim-web-devicons', opt = true },
	}

	-- Visualizador de imágenes, videos, PDF, LaTeX
	--use 'mbpowers/nvimager'
	-- Dependencias: ueberzug

	-- Fuzzy finder
	use {
		'nvim-telescope/telescope.nvim',
		requires = { {'nvim-lua/plenary.nvim'} }
	}

	use { 'nvim-telescope/telescope-file-browser.nvim', }

	-- Barras de estado y búfers
	use {
		'nvim-lualine/lualine.nvim',
		requires = {'kyazdani42/nvim-web-devicons', opt = true}
	}

	-- Plugin local para ver manuales en vim
	--use {
		--vim.env.XDG_DATA_HOME .. '/nvim/localplugins/pager',
		--branch = 'python_en_dirs'
	--}

end)
--}}}
