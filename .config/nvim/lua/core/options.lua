#!/usr/bin/luajit

local o = vim.o
local wo = vim.wo
local api = vim.api

-- Configuración_general: {{{

o.syntax = 'on'
--api.nvim_command [[colorscheme gruvbox]]

-- Sintaxis SQL y HTML en strings de PHP
vim.g.php_sql_query = 1
vim.g.php_htmlInStrings = 1

o.compatible = false
o.encoding = 'UTF-8'
o.clipboard = 'unnamedplus'
--o.clipboard = vim.o.clipboard .. 'unnamedplus'
o.mouse = 'a'
wo.number = true
wo.relativenumber = true
o.numberwidth = 1
o.textwidth = 1000
o.scrolloff = 7
o.sidescrolloff = 20
o.wrap = false
o.title = true
o.history = 1000
o.cursorline = true
o.showtabline = 2
o.showmode = false
--o.hidden = true
o.confirm = true
--o.t_Co = '256'
o.autochdir = true -- Siempre estar en directorio de fichero abierto
--vim.cmd([[
--autocmd BufEnter * silent! lcd %:p:h
--]]) -- Por si algunos plugins no detectan el directorio actual correcto con autochdir

o.expandtab = false
o.shiftwidth = 4
o.smarttab = true
o.tabstop = 4
o.softtabstop = 4
o.smartindent = false
o.autoindent = false
--set filetype indent on
--o.shiftround = true

o.splitbelow = true
o.splitright = true
o.equalalways = false  -- No igualar ventanas al cerrar
o.fillchars = vim.o.fillchars .. 'vert: ,eob: ,' -- caracteres de división
--o.fillchars = vim.o.fillchars .. 'vert:░,eob: ,'
o.startofline = true
o.backspace = 'indent,eol,start'
o.errorbells = false
o.visualbell = true
-- o.cursorcolumn = true
-- o.foldmethod = 'indent'
-- o.foldnestmax = 3
o.dictionary = '/usr/share/dict/spanish' -- Palabras
--o.dictionary = '/usr/share/dict/spanish' -- Tesauro
vim.cmd[[ set complete+=k ]]

o.ruler = true
o.wildmenu = true
o.hlsearch = true
o.incsearch = true
o.ignorecase = true
o.smartcase = true
o.termguicolors = true
o.showmatch = true
o.mat = 2
o.path = o.path .. '**'
o.dir = vim.env.HOME .. '/.cache/vim'
o.list = true
o.listchars = 'tab:➡ ,trail:•,extends:»,precedes:«'
--o.wildmenu = true
--o.omnifunc = 'syntaxcomplete#Complete'
--o.noswapfile = true
--o.backupdir = vim.env.HOME .. '/.cache/vim'
--o.nobackup = true
--o.undodir = vim.env.HOME .. '/.vim/undodir'
--o.undofile = true
--o.colorcolumn = 90
vim.g.asmsyntax = 'nasm'
vim.g.is_bash = 1
o.grepprg = 'rg --vimgrep --no-heading --smart-case $*' -- rg as grep program
o.grepformat = '%f:%l:%c:%m,%f:%l:%m'
--opt.shell = "/bin/bash"  -- Ensure bash as shell, not $SHELL
--vim.g.ps1_makeprg_cmd = "/usr/bin/pwsh"  -- Powershell as ps1 shell
vim.opt.lazyredraw = true -- Lazy redraw on macros and regexes

-- }}}
