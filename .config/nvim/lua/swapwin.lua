-- Swap windows in neovim
-- Based off https://github.com/wesQ3/vim-windowswap, to the author: there was no license, please contact me if you see this
-- Bug: it doesn't work with some special windows, like quickfix, help, dap, etc.
-- Look at: https://stackoverflow.com/questions/2586984/how-can-i-swap-positions-of-two-open-files-in-splits-in-vim
-- I'm dissapointed (Neo)vim doesn't have this by default and requires this "hack"

local fn = vim.fn
local nvim_exec = vim.api.nvim_exec

local SwapWin = {}

function SwapWin:set_mark()
	self.markedwinnum = {
		fn.tabpagenr(),
		fn.winnr()
	}
end

function SwapWin:del_mark()
	self.markedwinnum = nil
end

function SwapWin:swap_to_marked()
	--- Swap current window with marked window
	if self.markedwinnum ~= nil then
		-- Mark destination
		local current_tab  = fn.tabpagenr()
		local current_win  = fn.winnr()
		local current_view = fn.winsaveview()
		local current_buf  = fn.bufnr("%")
		local target_win   = self.markedwinnum
		nvim_exec('tabn '..target_win[1], true)
		nvim_exec(target_win[2]..'wincmd w', true)

		-- Switch to source and shuffle dest->source
		local marked_view = fn.winsaveview()
		local marked_buf  = fn.bufnr("%")

		-- Hide and open so that we aren't prompted and keep history
		nvim_exec('hide buf '..current_buf, true)
		fn.winrestview(current_view)

		-- Switch to dest and shuffle source->dest
		nvim_exec('tabn '..current_tab, true)
		nvim_exec(current_win..'wincmd w', true)

		-- Hide and open so that we aren't prompted and keep history
		nvim_exec('hide buf '..marked_buf, true)
		fn.winrestview(marked_view)

		self:del_mark()

	else
		error("No window marked to swap!",2)
	end

end

function SwapWin:swap_direction(key)
	--- Swap current window with another in certain key direction (h,j,k,l)
	self:set_mark()
	nvim_exec('wincmd '..key, true)
	self:swap_to_marked()
end

function SwapWin:mark_swap()
	if self.markedwinnum ~= nil then
		self:swap_to_marked()
	else
		self:set_mark()
	end
end

return SwapWin
