#!/usr/bin/luajit
local setkey1 = vim.api.nvim_set_keymap
local setkey2 = vim.keymap.set
local noremap = { noremap = true }

local movement_keys_remap = {
	-- Moviemento en caracteres
	c = {
		h = 'j',  -- ←
		j = 'l',  -- ↓
		k = 'k',  -- ↑
		l = 'ñ',  -- →
	},
	-- Moviemento en palabras
	w = {
		b = 'w',  -- ← palabra
		B = 'W',  -- ← PALABRA
		w = 'e',  -- → palabra
		W = 'E',  -- → PALABRA
		ge = 'B', -- ← Palabra
		e = 'b'   -- → Palabra
	}
}

--local mkr = movement_keys_remap
local mkrc = movement_keys_remap.c
local mkrw = movement_keys_remap.w

-- Remover mapeo si es igual
local remove_samekv = function(t)
	for k, v in pairs(t) do
		if k == v then
			t[k] = nil
		end
	end
end

--remove_samekv(movement_keys_remap.c)
--remove_samekv(movement_keys_remap.w)

-- Mapeo de teclas: {{{
-- Escape
setkey1('i','mñ','<Esc>', noremap)
setkey1('x','mñ','<Esc>', noremap)
--setkey1('c','<M-h>','<Esc>', noremap)

local function remap_main_keys()
	--- Remaper teclas principales de movimiento

	for oldkey,newkey in pairs(movement_keys_remap.c) do
		setkey1('n', newkey, oldkey, noremap)
		setkey1('x', newkey, oldkey, noremap)
		setkey1('n', 'c'..newkey, 'c'..oldkey, noremap)
		setkey1('n', 'd'..newkey, 'd'..oldkey, noremap)  -- No funciona con número prefijo? o sea 3dl corta a la derecha, reportar bug?
		setkey1('n', 'y'..newkey, 'y'..oldkey, noremap)
	end
	setkey1('i','<M-a>','<Right>',noremap)
	setkey1('i','<M-i>','<Left>',noremap)
	setkey1('i','<M-'..mkrc.l..'>','<Right>',noremap)
	setkey1('i','<M-'..mkrc.h..'>','<Left>',noremap)
	setkey1('i','<M-'..mkrc.j..'>','<Down>',noremap)
	setkey1('i','<M-'..mkrc.k..'>','<Up>',noremap)
	setkey1('c','<M-'..mkrc.l..'>','<Right>',noremap)
	setkey1('c','<M-'..mkrc.h..'>','<Left>',noremap)
	setkey1('i','<M-A>','<End>',noremap)
	setkey1('i','<M-BS>','<Del>',noremap)
	--setkey1('i','<M-I>','<Home>',noremap)  -- Esto va al inicio de línea, no inicio de palabras escritas

	setkey1('n','h',':',noremap)
	setkey1('x','h',':',noremap)

	setkey1('n','gl','gj',noremap)

end

-- WordMotion plugin: {{{
vim.g.wordmotion_mappings = {
	w = '<M-'..mkrw.w..'>',
	e = '<M-'..mkrw.e..'>',
	b = '<M-'..mkrw.b..'>',
	ge = 'g<M-'..mkrw.e..'>',
	W = '<M-'..mkrw.W..'>',
	B = '<M-'..mkrw.b..'>',
}
--}}}

-- Alternar entre mapeo con solo l,k o gl,gk en líneas envueltas
paragraphkeys = false
function vim.g.Paragraphkeys()
	if paragraphkeys then
		setkey1('n',mkrc.j,'j',noremap)
		setkey1('n',mkrc.k,'k',noremap)
		paragraphkeys = false
	else
		setkey1('n',mkrc.j,'gj',noremap)
		setkey1('n',mkrc.k,'gk',noremap)
		paragraphkeys = true
	end
end
--}}}

-- Movimiento entre palabras: {{{
local function remap_main_keys2()
	for oldkey,newkey in pairs(movement_keys_remap.w) do
		setkey1('n', newkey, oldkey, noremap)
		setkey1('x', newkey, oldkey, noremap)
		setkey1('n', 'd'..newkey, 'd'..oldkey, noremap)
		setkey1('n', 'c'..newkey, 'c'..oldkey, noremap)
		setkey1('n', 'y'..newkey, 'y'..oldkey, noremap)
	end
end
--}}}

local function remap_main_keys3()
	-- U rehacer. Ñ insertar_espacio. , por ;. - por / : {{{
	setkey1('n',',',';',noremap)
	setkey1('n',';',',',noremap)
	setkey1('x',',',';',noremap)
	setkey1('x',';',',',noremap)
	setkey1('n','U','<C-r>',noremap)
	setkey1('n','-','/',noremap)
	setkey1('x','-','/',noremap)
	setkey1('c','-','/',noremap) -- Más cómodo presionar - que / en mi teclado
	setkey1('c','/','-',noremap)
	setkey1('x','_',':S ',noremap)  -- Búsqueda solo en líneas seleccionadas (plugin vis.vim)
	setkey1('n','Y','y$',noremap)
	setkey1('n','Ñ','i<space><esc>',noremap)
	setkey1('n','M','K',noremap) -- Manuales
end
--}}}

-- Indentación: {{{
local function indentation_keymaps()
	setkey1('x','>','>gv',noremap)
	setkey1('x','<','<gv',noremap)
	setkey1('x','=','=gv',noremap)
	setkey1('x','|',':!unexpand -t',noremap) -- Reemplazar n espacios por tabulaciones
	-- s/\v^    (\w.*)/	\1/  # 4 espacios a tab
	-- s/\v^  (\w.*)/	\1/  # 2 espacios a tab
	-- s/\v^ +(\w.*)/	\1/
	setkey1('x','°',':!expand -t',noremap) -- Reemplazar tabulaciones por n espacios
	setkey1('x','<Tab>','>gv',noremap)
	setkey1('x','<S-Tab>','<gv',noremap)
	setkey1('i','<S-Tab>','<C-D',noremap)
	setkey1('n','<Tab>','>>_',noremap)
	setkey1('n','<S-Tab>','<<_',noremap)
	setkey1('x','z',':!column -ts= -o=',noremap) -- Alinear líneas en cierto caracter
end
--}}}

-- Divisiones (ventanas): {{{
local function window_keymaps()
	local swapwin = require'swapwin'
	setkey1('n','<M-f>o','<C-w>v',noremap) -- división horizontal
	setkey1('n','<M-f>i','<C-w>s',noremap) -- división vertical
	setkey1('n','<M-f>n','<C-w>n',noremap) -- crear división vacía
	setkey1('n','<M-f>w','<C-w>c',noremap) -- cerrar división
	setkey1('n','<M-f>'..mkrc.h,'<C-w>h',noremap) -- división ←
	setkey1('n','<M-f>'..mkrc.k,'<C-w>k',noremap) -- división ↑
	setkey1('n','<M-f>'..mkrc.j,'<C-w>j',noremap) -- división ↓
	setkey1('n','<M-f>'..mkrc.l,'<C-w>l',noremap) -- división →
	setkey2('n',"<M-q><M-q>", function() swapwin:mark_swap() end)
	setkey2('n',"<M-q><M-"..mkrc.h..">", function() swapwin:swap_direction("h") end) -- mover división ←
	setkey2('n',"<M-q><M-"..mkrc.j..">", function() swapwin:swap_direction("j") end) -- mover división ↑
	setkey2('n',"<M-q><M-"..mkrc.k..">", function() swapwin:swap_direction("k") end) -- mover división ↓
	setkey2('n',"<M-q><M-"..mkrc.l..">", function() swapwin:swap_direction("l") end) -- mover división →
	setkey1('n','<M-f>t','<C-w>T',noremap) -- división a pestaña

	setkey1('n','<M-f>0','<C-w>=',noremap) -- Igualar divisiones
	setkey1('n','<M-f>_','<C-w>_',noremap) -- Maximizar verticalmente
	setkey1('n','<M-f>|','<C-w>|',noremap) -- Maximizar horizontalmente
	setkey1('n','<M-f>r','<C-w>r',noremap) -- Rotar con siguiente división
	setkey1('n','<M-f>R','<C-w>R',noremap) -- Rotar con anterior división
	setkey1('n','<M-f>x','<C-w>x',noremap) -- Rotar entre 2 divisiones
	setkey1('n','<M-f>Ñ','<C-w>H',noremap) -- Intercambiar división vert a horiz
	setkey1('n','<M-f>J','<C-w>K',noremap) -- Intercambiar división horiz a vert
	setkey1('n',"<M-f><M-"..mkrc.h..">",':vertical resize -5<cr>',noremap)
	setkey1('n',"<M-f><M-"..mkrc.k..">",':resize +5<cr>',noremap)
	setkey1('n',"<M-f><M-"..mkrc.j..">",':resize -5<cr>',noremap)
	setkey1('n',"<M-f><M-"..mkrc.l..">",':vertical resize +5<cr>',noremap)
end
-- }}}

-- Pestañas: {{{
local function tab_keymaps()
	setkey1('n','<leader>n',':tabnew<CR>',{}) -- nueva pestaña
	setkey1('n','<leader>to',':tabonly<CR>',{}) -- cerrar todas pestañas menos actual
	setkey1('n','<leader>q',':tabclose<CR>',{}) -- cerrar pestaña actual (No está funcionando)
	--setkey1('n','<leader>,',':tabmove<cr>',{}) -- mover pestaña →
	setkey1('n','<leader>'..mkrc.l,':tabn<cr>',{}) -- pestaña →
	setkey1('n','<leader>'..mkrc.h,':tabp<cr>',{}) -- pestaña ←  // Menos fluido con mis mapeos de magma (remapear?)
end
-- }}}

-- Búfers: {{{
local function buffer_keymaps()
	setkey1('n','bn',':bn<CR>',{}) -- siguiente búfer, en búfer actual
	setkey1('n','b'..mkrc.l,':bn<CR>',{})
	setkey1('n','bp',':bp<CR>',{}) -- previo búfer, en búfer actual
	setkey1('n','b'..mkrc.h,':bp<CR>',{})
	setkey1('n','bd',':bd<CR>',{}) -- borra búfer
	setkey1('n','bo','<C-w>v:bn<CR>',{})  -- nueva división vertical con siguiente búfer
	setkey1('n','bi','<C-w>s:bn<CR>',{})  -- nueva división horizontal con siguiente búfer
end
--}}}

-- Misceláneas: {{{
setkey1('i','<M-d><M-f>','<C-x><C-f>',noremap)  -- Completado de rutas
setkey1('i','<M-d><M-k>','<C-x><C-k>',noremap)  -- Completado de diccionario
setkey1('i','<M-d><M-t>','<C-x><C-t>',noremap)  -- Completado de tesauro (sinónimos)
-- Remover resaltado de búsqueda
--setkey1('i','<C-m>',':nohl<CR>',noremap)
setkey1('x','<C-m>',':nohl<CR>',noremap)
setkey1('n','<Enter>',':nohl<CR>',noremap)
setkey1('n', "<leader>cd", ":cd %:h", noremap)

-- Resaltado mixto entre #vimsx=ft y #vimsx#
setkey1('n','<leader>sx',':call Sxs()<CR>',noremap)
-- Remover blancos finales, requiere script 'trim'
setkey1('n','<leader>sr',':.!trim -r<CR>',noremap)
setkey1('x','<leader>sr',':!trim -r<CR>',noremap)

-- Correr programas: {{{

-- Correr programa en nueva terminal
local function terminal_run(command, split_cmd, resize_cmd)
	split_cmd = split_cmd or "vnew"  -- TODO: validate that split_cmd is either new or vnew
	resize_cmd = resize_cmd or ""  --"vertical resize 50"
	vim.api.nvim_command(split_cmd)
	vim.api.nvim_command(resize_cmd)
	vim.api.nvim_command("term " .. command)
	vim.api.nvim_command("startinsert")
end

local function run_keymaps()
	-- Tengo un shell script 'compila' que compila/interpreta ficheros basado en su extensión
	setkey2('n','<leader>rr', function() terminal_run("compila -ir " .. vim.fn.expand('%:p')) end) -- Intepretar/Compilar y correr en búfer vertical
	setkey2('n','<leader>rh', function() terminal_run("compila -ir " .. vim.fn.expand('%:p'),"new") end) -- Intepretar/Compilar y correr en búfer horizontal
	setkey1('n','<leader>re',':!compila -ir "%:p"<CR>',noremap) -- Intepretar/Compilar y correr
	setkey1('n','<leader>rv',':so %<CR>',noremap) -- Recargar/Intepretar Neovim VimL/Lua
	setkey1('n','<leader>rw',':.!compila -ir "%:p"<CR>',noremap) -- Intepretar/Compilar y correr e insertar
	setkey1('n','<leader>rc',':!compila "%:p"<CR>',noremap) -- Compilar
	setkey1('n','<leader>ra',':!autocomp "%:p"<CR>',noremap) -- Alternar autocompilado. Bug, actualmente no funciona, la última vez que pasó fue por una actualización de entr
	setkey1('n','<leader>ri',':!"%:p"<CR>',noremap)  -- Ejecutar script actual ejecutable (con shebang)
	--setkey1('n','<leader>rw',':.!"%:p"<CR>',noremap) -- Ejecutar script actual ejecutable e insertar
	setkey1('n','<leader>rl',':%w !',noremap) -- Elegir comando al cual pasar fichero actual como entrada (e.g. un intérprete)
	setkey1('x','<leader>rl',':w !',noremap) -- Elegir comando al cual pasar líneas seleccionadas como entrada
	setkey1('n','<leader>rp','yy:exec \'r!\'@"<CR>',noremap) -- Ejecutar comando shell en línea actual, imprimir salida

	-- TODO:
	-- Que <leader>mr corra el fichero, habiendo expandido macros primero, pero sin reemplazar el fichero actual
	setkey1('n','<leader>rm',':w<CR>:%!m4s "%:p"<CR>',noremap) -- Preprocesar todo el fichero con m4
	setkey1('n','<leader>mm',':exec ".!m4s -t " . &filetype . "\"<CR>"',noremap)  -- Preprocesar línea con m4, y filetype. Problema: m4 lo lee __file__ como 'stdin'
	setkey1('x','<leader>m' ,':<C-u>exec "\'<,\'>!m4s -t " . &filetype . "\"<CR>"', noremap )
	setkey1('n','<leader>m4' ,':%!m4 "%:p"<CR>', noremap )
	--setkey1('v','<leader>m' ,':execute "!m4s -t " . &filetype . "<CR>"', noremap )
	--setkey1('n','<leader>mr')

	-- TODO:
	-- - Mapeo que en modo normal pueda pasar lo copiado (yy) a comando de shell, y pegar salida debajo
	-- - Mapeo que en modo visual pueda copie y pase a comando de shell como arriba ↑
	--setkey1('n','<leader>rp','yy:@" !',noremap)
end

-- }}}

-- Traducir
setkey1('n','<leader>ts',':.!argos-translate -f en -t es<CR>',noremap)  -- Inglés → Español
setkey1('n','<leader>tn',':.!argos-translate -f es -t en<CR>',noremap)  -- Español → Inglés
setkey1('x','<leader>ts', ':!argos-translate -f en -t es<CR>',noremap)  -- Inglés → Español
setkey1('x','<leader>tn', ':!argos-translate -f es -t en<CR>',noremap)  -- Español → Inglés

-- Pegar modeline al final
setkey1('n','<leader>ml',':call AppendModeline()<CR>',noremap)

-- Agregar/Remover líneas vacías
vim.cmd([[
nnoremap <silent><C-l> m`:silent +g/\m^\s*$/d<CR>``:noh<CR>
nnoremap <silent><C-k> m`:silent -g/\m^\s*$/d<CR>``:noh<CR>
nnoremap <silent><A-l> :set paste<CR>m`o<Esc>``:set nopaste<CR>
nnoremap <silent><A-k> :set paste<CR>m`O<Esc>``:set nopaste<CR>
]])

setkey1('n', "<M-P>", 'o<Esc>:.!xclip -o<CR>',noremap) -- Pegar portapapeles de X
--setkey1('n', "<leader>z", ":setlocal autoindent<CR>",{})
--setkey1('n', "<leader>Z", ":setlocal autoindent<CR>",{})

-- Convertir .py a notebooks Jupyter .ipynb
setkey1('n', "<leader>jn", ':!ipynb-py-convert "%:p" "%:r.ipynb"<CR>', noremap )

-- Mostrar PDF (Nota: rehacer esto pero en una función, no un atajo)
setkey1('n', "<leader>pp", ':!dem zathura "%:r.pdf"<CR><CR>', noremap)

-- # Al inicio de línea
setkey1('n', "<leader>x", "0<C-v><S-i>#<Esc>", {})

-- Seleccionar todo
setkey1('n', "<C-A>", "GVgg", noremap )

-- Teclas tipo Emacs
setkey1('i', "<C-K>", "<Esc>lDa", noremap ) -- Bug, cuando se está al principio de la línea, no corta primer caracter
--setkey1('i', "<C-U>", "<Esc>d0xi", noremap ) -- Bug, cuando se está al principio de la línea, corta primer caracter. Ya hay un mapeo por omisión
setkey1('i', "<C-Y>", "<Esc>Pa", noremap )
setkey1('i', "<C-A>", "<Home>", noremap )
setkey1('i', "<C-E>", "<End>", noremap )
--setkey1('i', "<C-B>", "<Left>", noremap )
--setkey1('i', "<C-F>", "<Right>", noremap )

-- }}}

-- Mapeo paquetes: {{{
-- Telescope
setkey1('n', "<leader>ff", "<cmd>Telescope find_files<cr>", noremap ) -- Buscar ficheros en directorio actual
setkey1('n', "<leader>fl", "<cmd>Telescope file_browser<cr>", noremap )
setkey1('n', "<leader>fg", "<cmd>Telescope live_grep<cr>", noremap ) -- Grep en todos los fichero en directorio actual
setkey1('n', "<leader>fb", "<cmd>Telescope buffers<cr>", noremap )  -- Buscar búfers
setkey1('n', "<leader>fh", "<cmd>Telescope help_tags<cr>", noremap )  -- Buscar manuales de ayuda de Vim/plugins
setkey1('n', "<leader>fk", "<cmd>Telescope keymaps<cr>", noremap ) -- Ver mapeos de teclas
setkey1('n', "<leader>fr", "<cmd>Telescope oldfiles<cr>", noremap ) -- Fichero recientes
setkey1('n', "<leader>fc", "<cmd>Telescope colorscheme<cr>", noremap )

-- Gitsigns
setkey1('n', "<leader>gsq", "<cmd>Gitsigns setqflist<cr>", noremap )
setkey1('n', "<leader>gsb", "<cmd>Gitsigns blame_line<cr>", noremap )
setkey1('n', "<leader>gsj", ":Gitsigns show ~", noremap )
setkey1('n', "<leader>gsd", "<cmd>Gitsigns diffthis<cr>", noremap )
setkey1('n', "<leader>gsn", "<cmd>Gitsigns next_hunk<cr>", noremap )
setkey1('n', "<leader>gsp", "<cmd>Gitsigns prev_hunk<cr>", noremap )
setkey1('n', "<leader>gss", "<cmd>Gitsigns stage_hunk<cr>", noremap )
setkey1('n', "<leader>gsS", "<cmd>Gitsigns stage_buffer<cr>", noremap )
setkey1('n', "<leader>gsu", "<cmd>Gitsigns undo_stage_hunk<cr>", noremap )
setkey1('n', "<leader>gsU", "<cmd>Gitsigns reset_buffer_index<cr>", noremap )

-- UndoTree
setkey1('n','<M-u>',':UndotreeToggle<CR>', noremap )

-- NvimTree
setkey1('n', "<C-f>", ":NvimTreeToggle<CR>", noremap )
setkey1('n', "<leader>r", ":NvimTreeRefresh<CR>", noremap )

-- Sniprun
setkey1('n','<leader>rs', ':%SnipRun<CR>', noremap)
setkey1('x','<leader>rs', ':SnipRun<CR>', noremap)

-- Nvimager: {{{
--nmap <leader>qq <Plug>NvimagerToggle
setkey1('n', "<leader>qq", ":NvimagerToggle<CR>", noremap )
--}}}

-- magma-vim (tipo-Jupyter)
--setkey1('n', "<leader>j", ":MagmaEvaluateOperator<CR>", noremap )
setkey1('n', "<leader>jj", ":MagmaEvaluateLine<CR>", noremap )
setkey1('x', "<leader>j", ":<C-u>MagmaEvaluateVisual<CR>", noremap )
setkey1('n', "<leader>jc", ":MagmaReevaluateCell<CR>", noremap )
setkey1('n', "<leader>jd", ":MagmaDelete<CR>", noremap )
setkey1('n', "<leader>jo", ":MagmaShowOutput<CR>", noremap )
vim.g.magma_wrap_output = false
vim.g.magma_save_path = "./.magma"

--vim.cmd([[
--"nnoremap <silent><expr> <leader>j  :MagmaEvaluateOperator<CR>
--"nnoremap <silent>       <leader>jr :MagmaEvaluateLine<CR>
--"xnoremap <silent>       <leader>j  :<C-u>MagmaEvaluateVisual<CR>
--"nnoremap <silent>       <leader>jc :MagmaReevaluateCell<CR>
--"nnoremap <silent>       <leader>jd :MagmaDelete<CR>
--"nnoremap <silent>       <leader>jo :MagmaShowOutput<CR>
--
--"let g:magma_automatically_open_output = v:false
--]])

-- Float Term {{{

-- FIXME: Cuando set autochdir = 1; no se cambia de directorio al abrir fichero
setkey1('n', "<M-.>", ":FloatermNew lf<CR>", noremap )
setkey1('n', "<M-,>", ":FloatermNew broot<CR>", noremap )

-- Nota: ueberzug adentro de floaterm parece no funcionar
vim.g.floaterm_keymap_new = "<M-T>"
vim.g.floaterm_keymap_toggle = "<M-t>"
vim.g.floaterm_keymap_next = "<M-S-p>"
vim.g.floaterm_keymap_prev = "<M-S-u>"

-- Salir modo insertar de terminal
setkey1('t','<M-Esc>','<C-\\><C-n>', noremap)

-- }}}

local function main()
	remap_main_keys()
	remap_main_keys2()
	remap_main_keys3()
	indentation_keymaps()
	window_keymaps()
	tab_keymaps()
	buffer_keymaps()
	run_keymaps()
end

main()
--}}}
