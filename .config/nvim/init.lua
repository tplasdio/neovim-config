#!/usr/bin/luajit
-- ██          ██   ██       ██
--░░          ░░   ░██      ░██
-- ██ ███████  ██ ██████    ░██ ██   ██  ██████
--░██░░██░░░██░██░░░██░     ░██░██  ░██ ░░░░░░██
--░██ ░██  ░██░██  ░██      ░██░██  ░██  ███████
--░██ ░██  ░██░██  ░██   ██ ░██░██  ░██ ██░░░░██
--░██ ███  ░██░██  ░░██ ░██ ███░░██████░░████████
--░░ ░░░   ░░ ░░    ░░  ░░ ░░░  ░░░░░░  ░░░░░░░░

local function main()
	require('core.disabler')
		.disable_builtin_plugins()

	-- Opciones de Vim
	require('core.options')

	-- Gestión de paquete
	require('core.pack')

	vim.g.mapleader = " "

	-- Mapeos de teclas
	require('keymap')

	-- Abreviaciones
	require('abbrev')

	require('core')
end

main()
