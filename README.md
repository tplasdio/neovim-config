<p align="center">
  <h1 align="center">Neovim-config</h1>
  <p align="center">
    My neovim configurations
  </p>
  <img
    width="1200"
    src="https://codeberg.org/tplasdio/neovim-config/raw/branch/main/media/neovim-config.avif"
    alt="image not loaded"
  />
</p>

## Table of Contents

- [📰 Description](#description)
- [✨ Features](#Features)
- [🚀 Setup](#setup)
- [📁 Configuration structure](#configuration-structure)
- [⌨ Keybindings](#keybindings)
  - [Normal mode](#normal-mode)
  - [Windows](#windows)
  - [Buffers](#buffers)
  - [Tabs](#tabs)
  - [Insert mode](#insert-mode)
  - [Plugins and Scripts](#plugins-and-scripts)
- [👀 See also](#see-also)
- [📝 Licence](#licence)

## 📰 Description

[Neovim](https://neovim.io) is a fast simple terminal text editor
that is nonetheless extremely configurable and extensible.

These are my personal configurations that make it
even more powerful and awesome.

## ✨ Features

- Configured in Lua
- Installed and configured plugins:

Plugin | Description
:---:|:---
[packer](https://github.com/wbthomason/packer.nvim) | Package manager
[undotree](https://github.com/mbbill/undotree) | Undo tree history
[floaterm](https://github.com/voldikss/vim-floaterm) | Floating terminal
[telescope](https://github.com/nvim-telescope/telescope.nvim) | Fuzzy finder
[telescope-file-browser](https://github.com/nvim-telescope/telescope-file-browser.nvim) | File browser
[lualine](https://github.com/nvim-lualine/lualine.nvim) | Status and tab lines
[cmp](https://github.com/hrsh7th/nvim-cmp) | Completions
[LSP](https://github.com/neovim/nvim-lspconfig) | Language Server Protocol configurations
[DAP](https://github.com/mfussenegger/nvim-dap) | Debug Adapter Protocol configurations
[DAP-UI](https://github.com/rcarriga/nvim-dap-ui) | DAP Neovim windows
[mkdnflow](https://github.com/jakewvincent/mkdnflow.nvim) | Wiki-like note taking with Markdown
[autopairs](https://github.com/jiangmiao/auto-pairs) | Automatic pairs
[vim-surround](https://github.com/tpope/vim-surround) | Pair surrounding
[nerdcommenter](https://github.com/preservim/nerdcommenter) | Commenting commands and keybinds
[vim-illuminate](https://github.com/RRethy/vim-illuminate) | Highlight homonymous words
[nvim-bqf](https://github.com/kevinhwang91/nvim-bqf) | Better quick fix window
[cmdbuf](https://github.com/notomo/cmdbuf.nvim) | Better command window
[gitsigns](https://github.com/lewis6991/gitsigns.nvim) | Signs, and hunk staging on git tracked files
[quick-scope](https://github.com/unblevable/quick-scope) | Character highting on f,F,t,T
[nvim-treesitter](https://github.com/nvim-treesitter/nvim-treesitter) | Syntax highlighting
[nvim-ts-rainbow](https://github.com/p00f/nvim-ts-rainbow) | Pairs coloring
[nvim-gdb](https://github.com/sakhnik/nvim-gdb) | Debugger for C, C++, Python, Bash, Nim
[magma-nvim](https://github.com/dccsillag/magma-nvim) | Jupyter execution for programming languages

- My gruvbox-inspired colorscheme 'pastelbox' that matches my AwesomeWM colorscheme. Has vibrant colors
- Other common nice defaults

## 🚀 Setup

- Install Neovim and the [packer](https://github.com/wbthomason/packer.nvim) package manager.
- Copy the `.config/nvim/*` files to your Neovim configuration directory
- Write `:PackerInstall` and hit enter. *Ignore the initial errors if any, it's normal*

Now the plugins should be installed.

It is also highly recommended:
- Install a font patched with [Nerd Fonts](https://www.nerdfonts.com). For example I use the [Fira Code](https://github.com/ryanoasis/nerd-fonts/tree/master/patched-fonts/FiraCode) font.
- Install any other dependencies of the plugins that packer doesn't manage like:
	- [`pynvim`](https://github.com/neovim/pynvim)
	- [`neovim-remote`](https://github.com/mhinz/neovim-remote)
	- [`magma-nvim`](https://github.com/dccsillag/magma-nvim) dependencies

For programming languages support:
- Install treesitter syntax highlighting by writing `:TSInstall <language>` for your language(s) and hit enter.
- Install LSP servers for your languages, these configurations work out of the box with:
	- [`pyright`](https://github.com/microsoft/pyright) for Python
	- [`clangd`](https://clang.llvm.org/) for C and C++ (comes with clang)
	- [`rust-analyzer`](https://rust-analyzer.github.io/) for Rust
	- [`bash-lsp`](https://github.com/bash-lsp/bash-language-server) for Bash
	- [`texlab`](https://texlab.netlify.com) for LaTeX
- Install DAP server for your debugging languages, these configurations work out of the box with:
	- [`debugpy`](https://aka.ms/debugpy) for Python
- There's also debugging support for:
	- `PDB` for Python (already comes with Python)
	- [`GDB`](https://www.gnu.org/software/gdb/) for C, C++ and Nim (already comes on many Linuxes or highly available)
	- [`BashDB`](http://bashdb.sourceforge.net/) for Bash and POSIX shell

*I've disabled some treesitter parsers that don't play well or aren't as matured*
*To add support for a new LSP or DAP, take a look at the files `.config/nvim/lua/plugins/configs/lang/{lsp,dap}.lua`*

With the FloatTerm plugin there's also support for the [`lf`](https://github.com/gokcehan/lf)
 file manager and the [`broot`](https://github.com/Canop/broot) fuzzy file picker inside
a floating terminal. You may also install them, here are my configs:
- [`lf-config`](https://codeberg.org/tplasdio/lf-config.git)
- [`broot-config`](https://codeberg.org/tplasdio/broot-config.git)

Finally a bit of functionality relies on some of my [scripts](https://codeberg.org/tplasdio/scripts/),
my [compila](https://codeberg.org/tplasdio/m4-snippets) compiler wrapper,
my [m4-snippets](https://codeberg.org/tplasdio/m4-snippets) snippets,
or other programs like seen in the configs.

***I'll work on improving the installation soon***

## 📁 Configuration structure

The file tree structure I went for this config looks like this:

```
~/.config/nvim
├── init.lua               ← Entry point for configurations
└── lua
    ├── abbrev
    │   └── init.lua       ← Neovim commands abbreviations
    ├── core
    │   ├── disabler.lua   ← Disabler of builtin Neovim plugins
    │   ├── init.lua       ← Other neovim configs: autocommands, color scheme
    │   ├── options.lua    ← Neovim main options configurations
    │   └── pack.lua       ← Installed packages configuration
    ├── keymap
    │   └── init.lua       ← Keybinding configuration
    ├── plugins
    │   └── configs
    │       ├── init.lua           ← Plugins configuration entry point
    │       ├── <plugin>.lua       ← Individual plugin configuration
    │       ├── completion
    │       │   └── <plugin>.lua   ← Individual completion plugins configuration
    │       └── lang
    │           └── <plugin>.lua   ← Individual language plugins configuration
    └── <script>.lua        ← Local scripts you may want to write go here
```

I may restructure it later. Feel free to hack around it.

*Note: Most of my comments are in Spanish*

*If you would like a single file configuration, take a look at the `single-file` branch of this repo.*

## ⌨ Keybindings

Since I use a Spanish keyboard I based my bindings
on my own custom vim movement keys:

 - `j` for ← instead of `h`
 - `k` for ↑ instead of `k`
 - `l` for ↓ instead of `j`
 - `ñ` for ← instead of `l`

If you won't use this style, simply change the remapping lua
table at the start of `lua/keymap/init.lua` to your liking.

Take a look at `lua/keymap/init.lua` for more details,
but here are the most important ones:

### Normal mode

Keybinding|Description
:---:|:---
`<leader>fk` | list keymaps
`space` | leader key
`j` | ←
`k` | ↑
`l` | ↓
`ñ` | →
`h` | command line
`w` | ← word
`W` | ← WORD
`B` | ← Word
`e` | → word
`E` | → WORD
`b` | → Word
`Y` | copy to left of line
`U` | redo
`Ñ` | insert space
`-`,`/` | search
`,` | next
`;` | previous
`alt-l` | insert blank line below
`alt-k` | insert blank line above
`ctrl-l` | remove blank line below
`ctrl-k` | remove blank line above

### Windows

Keybinding|Description
:---:|:---
`alt-f o` | open window right
`alt-f i` | open window below
`alt-f w` | close window
`alt-f j` | ← window
`alt-f k` | ↑ window
`alt-f l` | ↓ window
`alt-f ñ` | → window
`alt-f alt-j` | resize window ←
`alt-f alt-k` | resize window ↑
`alt-f alt-l` | resize window ↓
`alt-f alt-ñ` | resize window →
`alt-q alt-j` | swap window ←
`alt-q alt-k` | swap window ↑
`alt-q alt-l` | swap window ↓
`alt-q alt-ñ` | swap window →

### Buffers

Keybinding|Description
:---:|:---
`bd` | delete buffer
`bñ` | next buffer
`bn` | next buffer
`bj` | previous buffer
`bp` | previous buffer

### Tabs

Keybinding|Description
:---:|:---
`<leader>n` | new tab
`<leader>ñ` | next tab
`<leader>j` | previous tab

### Insert mode

Keybinding|Description
:---:|:---
`alt-j` | ←
`alt-k` | ↑
`alt-l` | ↓
`alt-ñ` | →
`alt-i` | ←
`alt-a` | →
`alt-I` | ← beginning of text
`alt-A` | ← end of line
`alt-backspace` | delete

### Plugins and Scripts

Keybinding|Description
:---:|:---
`<leader>fk` | telescope keymaps
`<leader>fl` | telescope file browser
`<leader>ff` | telescope file fuzzy finding
`<leader>fg` | telescope file grepping
`<leader>fr` | telescope recent files
`alt-u` | toggle undotree
`alt-t` | toggle floatterm
`alt-T` | new floatterm terminal
`alt-shift-u` | previous floatterm terminal
`alt-shift-p` | next floatterm terminal
`<leader>gss` | gitsigns stage hunk
`<leader>gsS` | gitsigns stage file
`<leader>gsu` | gitsigns unstage hunk
`<leader>gsU` | gitsigns unstage file
`K` | LSP show documentation of symbol
`gw` | LSP go to symbol
`gD` | LSP go to declaration
`gd` | LSP go to definition
`gr` | LSP go to reference of current symbol
`<leader>lr` | LSP rename symbol
`<leader>le` | LSP show line diagnostics
`<leader>b` | DAP/GDB break point
`<leader>dd` | DAP start
`<leader>db` | GDB start
`<leader>do` | DAP/GDB step over (next)
`<leader>di` | DAP/GDB step into
`<leader>dq` | DAP step out
`<leader>rr` | run script, output right
`<leader>rh` | run script, output below
`<leader>ra` | toggle autocompilation
`<leader>mm` | preprocess m4 macros
`alt-.` | floatterm open `lf` file manager
`alt-,` | floatterm open `broot` fuzzy file picker

## 👀 See also
- [Awesome-neovim](https://github.com/rockerBOO/awesome-neovim)

## 📝 Licence

GLPv3 or later. Look at each plugin for their individual licences.
